const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoose = require("./db.js");
const Grid = require("gridfs-stream");
const candidateController = require("./Controllers/candidateController.js");
const authController = require("./Controllers/authController");
const questionController = require("./Controllers/questionController");
const dataController = require("./Controllers/dataController");
const jobsController = require("./Controllers/jobsController");
const URI =
  "mongodb+srv://arpan:arpan123@candidates.xhujd.mongodb.net/employees?retryWrites=true&w=majority";

const port = process.env.PORT || 3000;

const app = express();

app.use(bodyParser.json());

// CORS HEADERS MIDDLEWARE
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header(
    "Access-Control-Allow-Methods",
    "GET, POST, HEAD, OPTIONS, PUT, PATCH, DELETE"
  );
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, x-access-token, x-refresh-token, _id"
  );
  res.header(
    "Access-Control-Expose-Headers",
    "x-access-token , x-refresh-token"
  );
  next();
});

const conn = mongoose.createConnection(
  URI,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  console.log("MongoDB Connected for files.")
);
let gfs;
conn.once("open", () => {
  gfs = Grid(conn.db, mongoose.mongo);
  gfs.collection("uploads");
});

// getting uploaded files.
app.get("/files/:filename", (req, res) => {
  gfs.files.findOne({ filename: req.params.filename + ".pdf" }, (err, file) => {
    console.log(file);
    if (!file || file.length === 0) {
      return res.status(404).json({
        err: "Error in fetching data.",
      });
    } else {
      res.send(file);
    }
  });
});

app.use("/candidate", candidateController);
app.use("/auth", authController);
app.use("/question", questionController);
app.use("/data", dataController);
app.use("/jobs", jobsController);

// app.use(express.static(path.join(__dirname + "/public")));

// app.get("*", (req, res) => {
//   res.sendFile(path.join(__dirname + "/public/index.html"));
// });

app.listen(port, () => console.log("server started at port : " + port));
