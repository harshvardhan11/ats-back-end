const express = require("express");
const jwt = require("jsonwebtoken");
var router = express.Router();
var ObjectId = require("mongoose").Types.ObjectId;
var { Job } = require("../models/job");
var { Candidate } = require("../models/candidate");
var { Skill } = require("../models/skill");
var { Country } = require("../models/country");
var { Industry } = require("../models/industry");
var { Question } = require("../models/question");
var { User } = require("../models/user.model");
const path = require("path");

const authMethods = require("../methods/authMethods");

const multer = require("multer");
var { QuestionPaper } = require("../models/questionPaper");
const Grid = require("gridfs-stream");
const GridFsStorage = require("multer-gridfs-storage");
const URI =
  "mongodb+srv://arpan:arpan123@candidates.xhujd.mongodb.net/employees?retryWrites=true&w=majority";

// Login & Signup
// check whether the request has a valid JWT access token
let authenticate = (req, res, next) => {
  let token = req.header("x-access-token");

  // verify JWT
  jwt.verify(token, User.getJWTSecret(), (err, decoded) => {
    if (err) {
      // there ws an error
      // jwt is invalid - * DO NOT AUTHENTICATE *
      res.status(401).send(err);
    } else {
      // jwt is valid
      req.user_id = decoded._id;
      next();
    }
  });
};

// Verify Refresh Token MiddleWare (which will be verifying the session)
let verifySession = (req, res, next) => {
  // grab the refresh token from header
  let refreshToken = req.header("x-refresh-token");

  // grab the _id from the request header
  let _id = req.header("_id");

  User.findByIdAndToken(_id, refreshToken)
    .then((user) => {
      if (!user) {
        // user couldn't be found
        return Promise.reject({
          error:
            "User not found. Make Sure that the refresh token and user id are correct",
        });
      }
      // if the code reaches here - the user was found
      // therefore the refresh token exists in the database - but we still have to check if it
      // has expired or not
      req.user_id = user._id;
      req.userObject = user;
      req.refreshToken = refreshToken;

      let isSessionValid = false;

      user.sessions.forEach((session) => {
        if (session.token === refreshToken) {
          // check if the session has expired
          if (User.hasRefreshTokenExpired(session.expiresAt) === false) {
            // refresh token has not expuired
            isSessionValid = true;
          }
        }
      });

      if (isSessionValid) {
        // the session is VALID - call next() to continue processing this web request
        next();
      } else {
        // the session is not valid
        return Promise.reject({
          error: "Refresh token has expired or the session is invalid",
        });
      }
    })
    .catch((e) => {
      res.status(401).send(e);
    });
};

module.exports = { authenticate, verifySession };
