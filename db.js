const mongoose = require("mongoose");
mongoose.set("useFindAndModify", false);
mongoose.connect(
  "mongodb+srv://arpan:arpan123@candidates.xhujd.mongodb.net/employees?retryWrites=true&w=majority",
  { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true },
  (err) => {
    if (!err) console.log("MongoDB connection succeeded.");
    else
      console.log(
        "Error in DB connection : " + JSON.stringify(err, undefined, 2)
      );
  }
);
module.exports = mongoose;
