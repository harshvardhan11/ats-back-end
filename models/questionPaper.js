const mongoose = require("mongoose");

var QuestionPaper = mongoose.model("QuestionPaper", {
  QuestionPaperName: { type: String },
});

module.exports = { QuestionPaper };
