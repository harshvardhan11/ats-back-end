const mongoose = require('mongoose');

var formName = mongoose.model('formName', {
	formName: { type: String },
});

module.exports = { formName };
