const mongoose = require("mongoose");

var Question = mongoose.model("Question", {
  Question: { type: String },
  Option1: { type: String },
  Option2: { type: String },
  Option3: { type: String },
  Option4: { type: String },
  Answer: { type: String },
  _QuestionPaperId: {
    type: mongoose.Types.ObjectId,
    required: true,
  },
});

module.exports = { Question };
