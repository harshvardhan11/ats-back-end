const mongoose = require("mongoose");
var Job = mongoose.model("Job", {
  PostingTitle: { type: String },
  Title: { type: String },
  Recruiter: { type: String },
  TargetDate: { type: String },
  JobStatus: { type: String },
  Salary: { type: String },
  SkillsRequired: { type: String },
  Department: { type: String },
  HiringManager: { type: String },
  NumberOfPositions: { type: Number },
  DateOpened: { type: String },
  JobType: { type: String },
  WorkExperience: { type: String },
  City: { type: String },
  Country: { type: String },
  State: { type: String },
  PostalCode: { type: Number },
  JobDescription: { type: String },
  Requirements: { type: String },
  Benefits: { type: String },
});
module.exports = { Job };
