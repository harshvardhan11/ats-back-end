const mongoose = require("mongoose");

var Skill = mongoose.model("Skill", {
  Skill: { type: String },
});

module.exports = { Skill };
