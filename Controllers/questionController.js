const express = require("express");
const router = express.Router();
const ObjectId = require("mongoose").Types.ObjectId;
const { Question } = require("../models/question");
const authMethods = require("../methods/authMethods");
const { QuestionPaper } = require("../models/questionPaper");

// get single question
router.get("/question/:questionId", authMethods.authenticate, (req, res) => {
  Question.findById({
    _id: req.params.questionId,
  })
    .then((question) => {
      res.send(question);
    })
    .catch((error) => {
      res.send(error);
    });
});

// post question
router.post(
  "/questionPaper/:paperId/question",
  authMethods.authenticate,
  (req, res) => {
    var question = new Question({
      Question: req.body.Question,
      Option1: req.body.Option1,
      Option2: req.body.Option2,
      Option3: req.body.Option3,
      Option4: req.body.Option4,
      Answer: req.body.Answer,
      _QuestionPaperId: req.params.paperId,
    });
    question.save((err, doc) => {
      if (!err) {
        res.send(doc);
      } else {
        console.log(
          "Error in Question Save:" + JSON.stringify(err, undefined, 2)
        );
      }
    });
  }
);

//edit question
router.put(
  "/questionPaper/:paperId/question/:id",
  authMethods.authenticate,
  (req, res) => {
    if (!ObjectId.isValid(req.params.id))
      return res.status(400).send(`No records with given id:${req.params.id}`);
    var updatedQuestion = {
      Question: req.body.Question,
      Option1: req.body.Option1,
      Option2: req.body.Option2,
      Option3: req.body.Option3,
      Option4: req.body.Option4,
      Answer: req.body.Answer,
      _QuestionPaperId: req.params.paperId,
    };
    Question.findByIdAndUpdate(
      req.params.id,
      { $set: updatedQuestion },
      { new: true },
      (err, doc) => {
        if (!err) {
          res.send(doc);
        } else {
          console.log(
            "Error in question update:" + JSON.stringify(err, undefined, 2)
          );
        }
      }
    );
  }
);

//delete question
router.delete(
  "/questionPaper/:paperId/question/:id",
  authMethods.authenticate,
  (req, res) => {
    Question.findOneAndRemove({
      _id: req.params.id,
    }).then((removedQuestion) => {
      res.send(removedQuestion);
    });
  }
);

// get all questions of selected question paper
router.get(
  "/questionPaper/:paperId/questions",
  authMethods.authenticate,
  (req, res) => {
    Question.find({ _QuestionPaperId: req.params.paperId }).then((response) => {
      res.send(response);
    });
  }
);

//get question paper
router.get("/questionPaper/:paperId", authMethods.authenticate, (req, res) => {
  QuestionPaper.findById({ _id: req.params.paperId }).then((response) => {
    res.send(response);
  });
});

//post question paper
router.post("/questionPaper", authMethods.authenticate, (req, res) => {
  var questionPaper = new QuestionPaper({
    QuestionPaperName: req.body.QuestionPaperName,
  });
  questionPaper.save((err, doc) => {
    if (!err) {
      res.send(doc);
    } else {
      console.log(
        "Error in Question Paper Save:" + JSON.stringify(err, undefined, 2)
      );
    }
  });
});

//edit question paper
router.put("/questionPaper/:paperId", authMethods.authenticate, (req, res) => {
  if (!ObjectId.isValid(req.params.paperId))
    return res
      .status(400)
      .send(`No records with given id:${req.params.paperId}`);
  var updatedQuestionPaper = {
    QuestionPaperName: req.body.QuestionPaperName,
  };
  QuestionPaper.findByIdAndUpdate(
    req.params.paperId,
    { $set: updatedQuestionPaper },
    { new: true },
    (err, doc) => {
      if (!err) {
        res.send(doc);
      } else {
        console.log(
          "Error in paper update:" + JSON.stringify(err, undefined, 2)
        );
      }
    }
  );
});

//delete paper
router.delete("/questionPaper/:paperId", (req, res) => {
  QuestionPaper.findOneAndRemove({
    _id: req.params.paperId,
  }).then((removedQuestionPaper) => {
    res.send(removedQuestionPaper);

    //delete all questions of that paper
    deleteQuestionsFromPaper(removedQuestionPaper._id);
  });
});

let deleteQuestionsFromPaper = (_id) => {
  Question.deleteMany({
    _QuestionPaperId: _id,
  }).then(() => {
    console.log("Questions from " + _id + " were deleted successfully!");
  });
};

// get all question papers
router.get("/questionPapers", authMethods.authenticate, (req, res) => {
  QuestionPaper.find().then((response) => {
    res.send(response);
  });
});

// get all questions
router.get("/questions", authMethods.authenticate, (req, res) => {
  Question.find().then((response) => {
    res.send(response);
  });
});

module.exports = router;
