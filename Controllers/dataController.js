const express = require("express");
const router = express.Router();
const { Skill } = require("../models/skill");
const { Country } = require("../models/country");
const { Industry } = require("../models/industry");

//get industries
router.get("/industries", (req, res) => {
  Industry.find().then((response) => {
    res.send(response);
  });
});

//post industries
router.post("/industries", (req, res) => {
  var industry = new Industry({
    Industry: req.body.Industry,
  });
  industry.save((err, doc) => {
    if (!err) {
      res.send(doc);
    } else {
      console.log(
        "Error in Employee Save:" + JSON.stringify(err, undefined, 2)
      );
    }
  });
});

router.get("/countries", (req, res) => {
  Country.find().then((response) => {
    res.send(response);
  });
});

router.post("/countries", (req, res) => {
  var country = new Country({
    Country: req.body.Country,
  });
  country.save((err, doc) => {
    if (!err) {
      res.send(doc);
    } else {
      console.log(
        "Error in Employee Save:" + JSON.stringify(err, undefined, 2)
      );
    }
  });
});

router.get("/skills", (req, res) => {
  Skill.find().then((response) => {
    res.send(response);
  });
});

router.post("/skills", (req, res) => {
  var skill = new Skill({
    Skill: req.body.Skill,
  });
  skill.save((err, doc) => {
    if (!err) {
      res.send(doc);
    } else {
      console.log(
        "Error in Employee Save:" + JSON.stringify(err, undefined, 2)
      );
    }
  });
});

module.exports = router;
