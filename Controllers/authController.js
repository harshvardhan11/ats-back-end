const express = require("express");
const router = express.Router();
const { User } = require("../models/user.model");
const authMethods = require("../methods/authMethods");

/** USER ROUTES */
/**
 * POST /users
 * Purpose: Sign up
 */
router.post("/users", (req, res) => {
  // User sign up
  let body = req.body;
  let newUser = new User(body);

  newUser
    .save()
    .then(() => {
      return newUser.createSession();
    })
    .then((refreshToken) => {
      // Session has been created successfully
      // now we generate an access auth token for the user
      return newUser
        .generateAccessAuthToken()
        .then((accessToken) => {
          // access auth token generated successfully, now we retur an object containing the auth tokens
          return { accessToken, refreshToken };
        })
        .then((authTokens) => {
          // Now we construct and send the response to the user with their auth token in header and
          // the user object in the body
          res
            .header("x-refresh-token", authTokens.refreshToken)
            .header("x-access-token", authTokens.accessToken)
            .send(newUser);
        })
        .catch((e) => {
          res.status(400).send(e);
        });
    });
});

/**
 * POST /users/login
 * Purpose: Login
 */
router.post("/users/login", (req, res) => {
  let email = req.body.email;
  let password = req.body.password;

  User.findByCredentials(email, password)
    .then((user) => {
      return user
        .createSession()
        .then((refreshToken) => {
          // Session created successfully
          // now we generate an access auth token for the user

          return user.generateAccessAuthToken().then((accessToken) => {
            // Access auth token generated successfully, now we return an object cntaining the auth tokens
            return { accessToken, refreshToken };
          });
        })
        .then((authTokens) => {
          // Now we construct and send the response to the user with their auth token in header and
          // the user object in the body
          res
            .header("x-refresh-token", authTokens.refreshToken)
            .header("x-access-token", authTokens.accessToken)
            .send(user);
        });
    })
    .catch((e) => {
      res.status(400).send(e);
    });
});

/**
 * GET users/me/access-token
 * Purpose: generates and returns an access token
 */
router.get("/users/me/access-token", authMethods.verifySession, (req, res) => {
  // we know that the user/caller is authenticated and we have the user_id and user object availabele to us
  req.userObject
    .generateAccessAuthToken()
    .then((accessToken) => {
      res.header("x-access-token", accessToken).send({ accessToken });
    })
    .catch((e) => {
      res.status(400).send(e);
    });
});

/**
 * GET - users
 * Purpose: get all users
 */
router.get("/users", authMethods.authenticate, (req, res) => {
  User.find().then((response) => {
    res.send(response);
  });
});

// Get single user
router.get("/user/:id", authMethods.authenticate, (req, res) => {
  User.find({ _id: req.params.id }).then((response) => {
    res.send(response);
  });
});

//delete user
router.delete("/user/:id", authMethods.authenticate, (req, res) => {
  User.findOneAndRemove({
    _id: req.params.id,
  }).then((removedUser) => {
    res.send({ message: "User deleted successfully!" });
    res.send(removedUser);
  });
});

//change password
router.patch("/changePassword/:id", authMethods.authenticate, (req, res) => {
  let body = req.body;
  console.log(body);
  let updatedUser = new User(body);
  console.log(updatedUser);

  User.findByIdAndUpdate(req.params.id, { $set: updatedUser }, (err, doc) => {
    if (!err) {
      res.send(doc);
    } else {
      console.log("Error in job update:" + JSON.stringify(err, undefined, 2));
    }
  });
});

// change password method 2
router.put(
  "/changePassword/:id",
  authMethods.authenticate,
  async (req, res) => {
    const { error } = validateProduct(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const product = await Product.findById(req.params.id).exec();
    if (!product)
      return res
        .status(404)
        .send("The product with the given ID was not found.");

    let query = { $set: {} };
    for (let key in req.body) {
      if (product[key] && product[key] !== req.body[key])
        // if the field we have in req.body exists, we're gonna update it
        query.$set[key] = req.body[key];

      const updatedProduct = await Product.updateOne(
        { _id: req.params.id },
        query
      ).exec();
    }

    res.send(product);
  }
);

module.exports = router;
