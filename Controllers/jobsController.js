const express = require("express");
const router = express.Router();
const ObjectId = require("mongoose").Types.ObjectId;
const { Job } = require("../models/job");
const path = require("path");
const multer = require("multer");
const GridFsStorage = require("multer-gridfs-storage");
const URI =
  "mongodb+srv://arpan:arpan123@candidates.xhujd.mongodb.net/employees?retryWrites=true&w=majority";

router.get("/jobs", (req, res) => {
  Job.find().then((jobs) => {
    res.send(jobs);
  });
});

router.get("/jobs/:id", (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res.status(400).send(`No records with given id:${req.params.id}`);

  Job.findById(req.params.id, (err, doc) => {
    if (!err) {
      res.send(doc);
    } else {
      console.log(
        "Error in Retriving Employees:" + JSON.stringify(err, undefined, 2)
      );
    }
  });
});

router.get("/candidate/:id", (req, res, next) => {
  _id = req.params.id;
  console.log(id);

  const storage = new GridFsStorage({
    url: URI,
    file: (req, file) => {
      return new Promise((resolve, reject) => {
        const fileInfo = {
          filename: "" + id + path.extname(file.originalname),
          bucketName: "uploads",
        };
        resolve(fileInfo);
      });
    },
  });

  //create storage engine
  const upload = multer({ storage });

  // @route POST /upload
  // @desc Uploads file to DB.
  router.post("/upload", upload.single("resume"), (req, res) => {
    res.json();
  });
});

router.post("/jobs", (req, res) => {
  var job = new Job({
    PostingTitle: req.body.PostingTitle,
    Title: req.body.Title,
    Recruiter: req.body.Recruiter,
    TargetDate: req.body.TargetDate,
    JobStatus: req.body.JobStatus,
    Salary: req.body.Salary,
    SkillsRequired: req.body.SkillsRequired,
    Department: req.body.Department,
    HiringManager: req.body.HiringManager,
    NumberOfPositions: req.body.NumberOfPositions,
    DateOpened: req.body.DateOpened,
    JobType: req.body.JobType,
    WorkExperience: req.body.WorkExperience,
    City: req.body.City,
    Country: req.body.Country,
    State: req.body.State,
    PostalCode: req.body.PostalCode,
    JobDescription: req.body.JobDescription,
    Requirements: req.body.Requirements,
    Benefits: req.body.Benefits,
  });
  job.save((err, doc) => {
    if (!err) {
      res.send(doc);
    } else {
      console.log(
        "Error in Employee Save:" + JSON.stringify(err, undefined, 2)
      );
    }
  });
});

router.put("/jobs/:id", (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res.status(400).send("No records with given id:${req.params.id}");
  var job = {
    PostingTitle: req.body.PostingTitle,
    Title: req.body.Title,
    Recruiter: req.body.Recruiter,
    TargetDate: req.body.TargetDate,
    JobStatus: req.body.JobStatus,
    Salary: req.body.Salary,
    SkillsRequired: req.body.SkillsRequired,
    Department: req.body.Department,
    HiringManager: req.body.HiringManager,
    NumberOfPositions: req.body.NumberOfPositions,
    DateOpened: req.body.DateOpened,
    JobType: req.body.JobType,
    WorkExperience: req.body.WorkExperience,
    City: req.body.City,
    Country: req.body.Country,
    State: req.body.State,
    PostalCode: req.body.PostalCode,
  };
  Job.findByIdAndUpdate(
    req.params.id,
    { $set: job },
    { new: true },
    (err, doc) => {
      if (!err) {
        res.send(doc);
      } else {
        console.log("Error in job update:" + JSON.stringify(err, undefined, 2));
      }
    }
  );
});

router.delete("/jobs/:id", (req, res) => {
  // we want to delete the specified job
  Job.findOneAndRemove({
    _id: req.params.id,
  }).then((removedListDoc) => {
    res.send({ message: "Item deleted successfully!" });
  });
});

module.exports = router;
